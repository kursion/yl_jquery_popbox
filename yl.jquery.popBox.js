// Show a popBox when button clicked
jQuery.fn.popBox = function(popWindow, callbackFunc, canClose, hideOnStart){
  
  // Getting the name of the windows
  var popWindowName = popWindow.substring(1, popWindow.length);
  
  // Default values
  canClose = canClose || true;
  hideOnStart = hideOnStart || true;
  callbackFunc = callbackFunc || null;
    
	// Adding class popBox
	$(popWindow).addClass('yl-jquery-popBox');
	
  // Adding tags
  $(popWindow).html("<div class='text'>"+$(popWindow).html()+"</div>");
  
	// Adding close button once
  $(popWindow).prepend("<img src='btn_close.png' class='yl-jquery-popBox_close_button' id='yl-jquery-popBox_close_button'>");
  if(!canClose){ $(popWindow+" #yl-jquery-popBox_close_button").hide(); }
    
	// Adding background
  if(!$("#yl-jquery-popBox_background_"+popWindowName).length){
      $("body").after("<div id='yl-jquery-popBox_background_"+popWindowName+"' class='yl-jquery-popBox_background'></div>");
  }
	
	// On click show popBox
	$(this).click(function(){
		$('html, body').scrollTop(0);
		
    setTimeout(function(){ 
			$("#yl-jquery-popBox_background_"+popWindowName).css({"opacity" : "0.7"}).fadeIn("normal");
			$("#yl-jquery-popBox_background_"+popWindowName).css("height", $("#body").height() );
			$(popWindow).fadeIn("normal");
      $(popWindow).css("top", "100px");
      $(popWindow).css("left", ( $('body').width()-$(popWindow).width()) / 2+$(popWindow).scrollLeft() + "px");
		}, 100);
		
		return this;
	});
	
  // The function that will be called to close the popBox
  function closePopBox(){
    $("#yl-jquery-popBox_background_"+popWindowName).fadeOut("fast");
    $(popWindow).fadeOut("fast");
    if(callbackFunc){ callbackFunc(); }
  }
  
	// On Key Pressed hide popBox
	$('body').keypress(function(e){ if(e.keyCode==27){ closePopBox(); }});
	
	// On close cross clicked hide popBox
	$(function(){
		$(popWindow+" #yl-jquery-popBox_close_button").click(function(){ closePopBox(); });
	});
}